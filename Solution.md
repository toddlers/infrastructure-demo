## Environment

 * `kubectl` is on the path
 * A Kubernetes cluster is set up and the the default `kubectl` context points to that cluster. 
   We can create the cluster using below commands

   ```
   gcloud config set project my-project-id
   gcloud config set compute/zone us-central1-f
   gcloud container clusters create snaptravel
   gcloud container clusters get-credentials --cluster snaptravel
   ```

## Building the Docker Image
 
 * Do a git init in the code and set respective user and email address
  ```
  git config --global user.email "[EMAIL_ADDRESS]"
  git config --global user.name "[USERNAME]"
  ```
  
 * Make initial commits to the source code repository
 * Create a repository to host your code
 ```
  gcloud source repos create sample-app
  git config credential.helper gcloud.sh
 ```
 * Add your newly created repository as remote
 ```
 export PROJECT=$(gcloud info --format='value(config.project)')
 git remote add origin https://source.developers.google.com/p/$PROJECT/r/snaptravel
 ```
 * Push your code to the new repository's master branch
 ```
 git push origin master
 ```
 
 * Create a Tag for the code and push 
 ```
 git tag v1.0.0
 git push --tags
 ```
 
 
### Configuring the build triggers :
 
 * In GCP console clikc `Build Triggers` in the container registry seciotn
 * Select `Cloud Source Repository` and click `Continue`
 * Select snaptravel repository and select cloudbuild.yaml location: `/cloudbuild.yaml`.
 * Select Trigger type as `Tag` and in the Tag regex provide `v.*`


## Deployment

 * There is a file named `snaptravel.yaml` . That is Kubernetes manifest for the project.  
   Deploy the app using below command
   ```
   kubectl create -f snaptravel.yml
   ```
 * Check respective services and pods using commands
 
 ```
 ⚡ kubectl get svc # getting service
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)        AGE
db           ClusterIP      10.63.253.95    <none>          5432/TCP       2h
kubernetes   ClusterIP      10.63.240.1     <none>          443/TCP        5h
web          LoadBalancer   10.63.244.128   35.232.XXX.XXX   80:30005/TCP   2h
 
 
 ⚡ kubectl  get pods # getting pods
NAME                              READY     STATUS    RESTARTS   AGE
db                                1/1       Running   0          2h
web-controller-74ddb79c9b-ckzt2   1/1       Running   0          28m
web-controller-74ddb79c9b-f5mqx   1/1       Running   0          28m
web-controller-74ddb79c9b-f7lrx   1/1       Running   0          27m
web-controller-74ddb79c9b-nttqq   1/1       Running   0          27m

 ```
 *  Now we are considering we dont have to open the app to the world . So I'll create specific firewall rules.
 
 ```
 git:master *⚡ kubectl  get nodes
NAME                                  STATUS    ROLES     AGE       VERSION
gke-snaptravel-default-pool-6d016fb9-7phh   Ready     <none>    5h        v1.8.8-gke.0
gke-snaptravel-default-pool-6d016fb9-97q5   Ready     <none>    5h        v1.8.8-gke.0
gke-snaptravel-default-pool-6d016fb9-h87w   Ready     <none>    5h        v1.8.8-gke.0

 ```
 
 * To create and list the firewall rule that allows traffic into the cluster I use a gcloud compute command 
 
 ```
 gcloud compute firewall-rules create --allow=tcp:80 --target-tags=gke-snaptravel-default-pool-6d016fb9 snaptravel #creating the firewall rule
 gcloud compute forwarding-rules list
 
 ```
 
## Autoscaling
 
 * When you use kubectl autoscale, you specify a maximum and minimum number of replicas for your application, 
   as well as a CPU utilization target. For example, to set the maximum number of replicas to six and the minimum 
   to four, with a CPU utilization target of 50% utilization, run the following command:
   
```
   git:master *⚡ kubectl  get deployments  # getting deployments list
     NAME             DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
     web-controller   4         4         4            4           3h

   git:master *%=⚡ kubectl autoscale deployment web-controller --max 6 --min 4 --cpu-percent 50
    deployment "web-controller" autoscaled

  git:master *⚡ kubectl get hpa
    NAME             REFERENCE                   TARGETS    MINPODS   MAXPODS   REPLICAS   AGE
    web-controller   Deployment/web-controller   0% / 50%   4         6         4          1h
  git:master *⚡ kubectl get pods
   NAME                              READY     STATUS    RESTARTS   AGE
   db                                1/1       Running   0          3h
   web-controller-74ddb79c9b-ckzt2   1/1       Running   0          33m
   web-controller-74ddb79c9b-f5mqx   1/1       Running   0          33m
   web-controller-74ddb79c9b-f7lrx   1/1       Running   0          32m
   web-controller-74ddb79c9b-nttqq   1/1       Running   0          32m
  
```
   


## Deployment
 * Do the necessary code changes , tag them and push to the repo. 
   Once pushed we can check the auto build in the build history
 * We have three ways to do rolling update.
   - Using `set image`
   
     ```
     # format
     $ kubectl set image deployment <deployment> <container>=<image> --record
     # example
     git:mastkubectl set image deployment web-controller  web=snaptravel:v1.0.3 --record
     deployment "web-controller" image updated
     
     ```
     
  - Modify the kubernetes manifest and update the docker image version.
  
    ```
    # format
    $ kubectl replace -f <yaml> --record
    # example
    $ kubectl replace -f snaptravel.yml --record
    
    ```
   
  - Using `Edit`
  
    ```
    # format
    $ kubectl edit deployment <deployment> --record
    git:master *⚡ kubectl edit deployment web-controller --record
    
    ```
 * Check Status/Pause/Resume Deployments
 
   ```
   kubectl rollout status deployment web-controller # Check Status
   kubectl rollout pause deployment web-controller # Pause Rolling Update
   kubectl rollout resume deployment web-controller # Resume Rolling Update
   
   ```
   
## Rollback
  # to previous revision
  
  ```
   $ kubectl rollout undo deployment web-controller
   git:master *⚡ kubectl rollout undo deployment web-controller
   deployment "web-controller" rolled back
   git:master *⚡
 ```
  # to specific revision
  
 ```
   $ kubectl rollout undo deployment web-controller --to-revision=<revision>
   # exmaple
   $ kubectl rollout undo deployment web-controller --to-revision=v1.0.0
 
 ```
   
## Future Improvements
 
 * We can have a dedicated postgres cluster in google cloud.
 * Create a full continous deployments pipeline using `Spinnaker`.
 * Corresponding `cloudbuild.yaml` for building the image, can be extended to
   run all kinds of tests.
 * Kubernetes Cluster can be in `auto scaling` mode depending on various params.
 * All the deployed containers can be limited with memory and CPU.
 * We can use `vault` or `kubernetes secrets` for storing various credentials.
 * For Monitoring we can have `Prometheus` etc.
